#pragma once
#include <any>
#include <array>
#include <string>
#include "IObject.hpp"


namespace tanki {



class IMovable {
public:
  virtual std::array<double, 3> &get_position() = 0;
  virtual std::array<double, 3> &get_velocity() = 0;

  virtual void set_position(std::array<double, 3> position) = 0;
  virtual void set_velocity(std::array<double, 3> velocity) = 0;

  //virtual IObject *get_object()=0;

  virtual ~IMovable() = default;
};

} // namespace tanki
