#pragma once

#include "MoveCommand.hpp"
#include "core/ICommand.hpp"
#include "core/IMovable.hpp"
#include "core/IQueue.hpp"
#include "core/IObject.hpp"
#include <iostream>
#include "EmptyCommand.hpp"


namespace tanki {
class EndMoveCommand : public ICommand {
  IObject *_obj;

public:
  EndMoveCommand(IObject *obj) : _obj(obj) {}

  void execute() override {

    auto cmd = new EmptyCommand();
    _obj->set_property("MoveCommand", cmd);

  }
};
} // namespace tanki
