#include "Mocks.hpp"
#include "gtest/gtest.h"
#include "core/IObject.hpp"

TEST(MoveCommandsTest, StartMoveTest) {
  MockMovable movable;
  MockQueue mock_queue;
  IObject *_obj;

  EXPECT_CALL(movable, set_velocity(std::array<double, 3>{1.0, 0.0, 0.0}))
      .Times(1);


  EXPECT_CALL(mock_queue, add_command(testing::_))
      .Times(1);

  StartMoveCommand command(&movable, {1.0, 0.0, 0.0}, &mock_queue,_obj);
  command.execute();
}


TEST(MoveCommandsTest, MoveTest) {
  MockMovable movable;

  auto velocity = std::array<double, 3>{1.0, 3242.3, 123132222556.0};
  auto position = std::array<double, 3>{234., 256., 312.};

  auto start_position = position;
  auto end_position = position;

  for (unsigned int i = 0; i < 3; i++) {
    end_position[i] += velocity[i];
  }

  EXPECT_CALL(movable, get_velocity()).WillOnce(testing::ReturnRef(velocity));
  EXPECT_CALL(movable, get_position()).WillOnce(testing::ReturnRef(position));

  MoveCommand command(&movable);
  command.execute();

  EXPECT_EQ(position, end_position);
  EXPECT_NE(position, start_position);
}

TEST(MoveCommandsTest, EndMoveTest) {
  MockMovable movable;
  MockQueue mock_queue;
  IObject *obj;


  EXPECT_CALL(movable, set_velocity(std::array<double, 3>{0.0})).Times(1);

  //EXPECT_CALL(mock_queue, remove_command()).Times(1);

  EndMoveCommand command(obj);
  command.execute();
}
