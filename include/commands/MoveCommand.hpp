#pragma once

#include "core/ICommand.hpp"
#include "core/IMovable.hpp"
#include "core/IQueue.hpp"
#include <iostream>

namespace tanki {

class MoveCommand : public ICommand {
  IMovable *object;

public:
  MoveCommand(IMovable *obj) { this->object = obj; };

  void execute() override {
    auto &velocity = this->object->get_velocity();
    auto &position = this->object->get_position();

    for (int i = 0; i < 3; i++) {
      position[i] += velocity[i];
    }
  }
};

} // namespace tanki
