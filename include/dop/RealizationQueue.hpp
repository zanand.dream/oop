#pragma once

#include "../core/IQueue.hpp"
#include <iostream>


#include "../core/ICommand.hpp"
#include <queue>
#include <string>

class RealizationQueue : public IQueue {

private:
  std::queue<tanki::ICommand *> _queue{};

public:
  void add_command(tanki::ICommand *command) override { _queue.push(command); }
  void remove_command() override { _queue.pop(); }
};