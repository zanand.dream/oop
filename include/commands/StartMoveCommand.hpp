#pragma once

#include "MoveCommand.hpp"
#include "core/ICommand.hpp"
#include "core/IMovable.hpp"
#include "core/IQueue.hpp"
#include <iostream>
#include "core/IObject.hpp"

namespace tanki {

  class MoveObjectCommand : public ICommand {
  IObject* m_obj;

  public:

  void execute() {
    auto cmd = std::any_cast<ICommand*>(m_obj->get_property("MoveCommand"));

    cmd->execute();
  }
};


class StartMoveCommand : public ICommand {

  IMovable *_move;
  IQueue *_queue;
  std::array<double, 3> velocity;
  IObject *_obj;

public:
  StartMoveCommand(IMovable *move, std::array<double, 3> velocity, IQueue *queue, IObject *obj)
      : _move(move), _queue(queue), velocity(velocity),_obj(obj){};

  void execute() override {

    auto cmd = new MoveCommand(_move);

    this->_obj->set_property("MoveCommand", cmd);

    this->_move->set_velocity(this->velocity);

    this->_queue->add_command(new MoveObjectCommand());


  }
};
} // namespace tanki
