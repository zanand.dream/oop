#pragma once

#include "gmock/gmock.h"
#include <any>
#include <array>
#include <gmock/gmock-function-mocker.h>
#include <string>

#include "commands/EndMoveCommand.hpp"
#include "commands/MoveCommand.hpp"
#include "commands/StartMoveCommand.hpp"

#include "core/IMovable.hpp"
#include "core/IQueue.hpp"
#include "core/IObject.hpp"

// using ::testing::ReturnRef;

using namespace tanki;

class MockMovable : public IMovable {
public:
  MOCK_METHOD(IObject *, get_object, ());

  MOCK_METHOD((std::array<double, 3> &), get_position, (), (override));

  MOCK_METHOD(void, set_position, ((std::array<double, 3> postion)),
              (override));

  MOCK_METHOD((std::array<double, 3> &), get_velocity, (), (override));

  MOCK_METHOD(void, set_velocity, ((std::array<double, 3> position)),
              (override));
};

class MockQueue : public IQueue {
public:
  MOCK_METHOD((void), add_command, (tanki::ICommand * command), (override));
  MOCK_METHOD((void), remove_command, (), (override));
};

class MockIObject: public IObject{
  public:
    MOCK_METHOD((std::any), get_property, (const std::string &key), (override));
    MOCK_METHOD(void, set_property, (const std::string &key, const std::any &value), (override));
};
