#pragma once

namespace tanki {

class ICommand {
public:
  virtual void execute() = 0;

  virtual ~ICommand() = default;

};

}; // namespace tanki
