#pragma once

#include <string>
#include <any>

namespace tanki{

class IObject {
public:
  virtual std::any get_property(const std::string& key) = 0;
  virtual void set_property(const std::string& key, const std::any& value) = 0;
};
}
