#pragma once

#include "core/ICommand.hpp"

class EmptyCommand : public tanki::ICommand
{
public:
	void execute()
	{}
};
