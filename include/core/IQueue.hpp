#pragma once

#include <memory>
#include <string>

#include "ICommand.hpp"

using namespace std;

class IQueue {
    public:
  virtual void add_command(tanki::ICommand *command) = 0;
  
  virtual void remove_command() = 0;
};